<?php

namespace AzureSpring\UmpOpenAPI;

use GuzzleHttp\Client as Guzzle;

class Client
{
    const BASE_URI = 'https://gateway.open.umeng.com/openapi/';

    private $guzzle;

    private $apiKey;

    private $apiSecret;

    private $appKey;

    public function __construct(Guzzle $guzzle, string $apiKey, string $apiSecret, string $appKey)
    {
        $this->guzzle = $guzzle;
        $this->apiKey = $apiKey;
        $this->apiSecret = $apiSecret;
        $this->appKey = $appKey;
    }

    /**
     * https://developer.umeng.com/docs/147615/detail/162430
     */
    public function findEvents()
    {
        $data = $this->request('com.umeng.umini:umeng.umini.getEventList-1', []);

        return array_map(
            function ($x) {
                return $x['eventName'];
            },
            $data
        );
    }

    /**
     * https://developer.umeng.com/open-api/docs/com.umeng.umini/umeng.umini.getEventProvertyList/1
     */
    public function findProperties(string $event)
    {
        $data = $this->request('com.umeng.umini:umeng.umini.getEventProvertyList-1', ['eventName' => $event]);

        return array_map(
            function ($x) {
                return $x['propertyName'];
            },
            $data
        );
    }

    /**
     * https://developer.umeng.com/open-api/docs/com.umeng.umini/umeng.umini.getEventOverview/1
     */
    public function countEvents(string $event, Model\Interval $interval)
    {
        $data = $this->request('com.umeng.umini:umeng.umini.getEventOverview-1', $interval->toParams() + [
            'eventName' => $event,
        ]);

        return Model\Fragment::create($data, [Model\PeriodicCount::class, 'create']);
    }

    /**
     * https://developer.umeng.com/open-api/docs/com.umeng.umini/umeng.umini.getAllPropertyValueCount/1
     */
    public function countEventsByProperty(string $event, string $property, Model\Interval $interval, ?Model\Page $page = null)
    {
        if (!$page) {
            $page = new Model\Page();
        }
        $data = $this->request('com.umeng.umini:umeng.umini.getAllPropertyValueCount-1', $interval->toParams() + $page->toParams() + [
            'eventName' => $event,
            'propertyName' => $property,
        ]);

        return Model\Fragment::create($data, [Model\PropertiedCount::class, 'create']);
    }

    /**
     * https://developer.umeng.com/docs/147615/detail/162430
     */
    private function request(string $name, array $params)
    {
        if (!preg_match('/^(?P<namespace>.*):(?P<name>.*)-(?P<version>.*)$/', $name, $matches)) {
            throw new \LogicException();
        }

        $path = implode('/', ['param2', $matches['version'], $matches['namespace'], $matches['name'], $this->apiKey]);
        $params += ['dataSourceId' => $this->appKey];
        $params += ['_aop_signature' => $this->sign($path, $params)];

        $response = $this->guzzle->get($path, ['query' => $params]);
        $document = \GuzzleHttp\json_decode($response->getBody(), true);
        if (!$document['success']) {
            throw new Exception\ServiceException($document['msg'], $document['code']);
        }

        return $document['data'];
    }

    /**
     * https://developer.umeng.com/docs/147615/detail/67643
     */
    private function sign(string $path, array $params)
    {
        $pstr = array_map(
            function ($key, $value) {
                return implode([$key, $value]);
            },
            array_keys($params),
            array_values($params)
        );
        sort($pstr, SORT_STRING);

        return strtoupper(hash_hmac('sha1', $path.implode($pstr), $this->apiSecret));
    }
}
