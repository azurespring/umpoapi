<?php

namespace AzureSpring\UmpOpenAPI\Exception;

class ServiceException extends \RuntimeException implements Exception
{
}
