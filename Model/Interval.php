<?php

namespace AzureSpring\UmpOpenAPI\Model;

class Interval
{
    private $begin;

    private $end;

    private $granularity;

    public function __construct(\DateTimeInterface $begin, \DateTimeInterface $end, $granularity = 'day')
    {
        $this->begin = $begin;
        $this->end = $end;
        $this->granularity = $granularity;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getBegin(): \DateTimeInterface
    {
        return $this->begin;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEnd(): \DateTimeInterface
    {
        return $this->end;
    }

    /**
     * @return string
     */
    public function getGranularity(): string
    {
        return $this->granularity;
    }

    public function toParams()
    {
        return [
            'fromDate' => $this->begin->format('Y-m-d'),
            'toDate' => $this->end->format('Y-m-d'),
            'timeUnit' => $this->granularity,
        ];
    }
}
