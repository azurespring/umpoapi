<?php

namespace AzureSpring\UmpOpenAPI\Model;

class PropertiedCount
{
    private $value;

    private $count;

    private $percentage;

    public static function create($data)
    {
        return new PropertiedCount($data['propertyValue'], $data['count'], $data['countRatio']);
    }

    public function __construct(string $value, int $count, float $percentage)
    {
        $this->value = $value;
        $this->count = $count;
        $this->percentage = $percentage;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return float
     */
    public function getPercentage(): float
    {
        return $this->percentage;
    }
}
