<?php

namespace AzureSpring\UmpOpenAPI\Model;

class Fragment
{
    private $data;

    private $page;

    private $total;

    public static function create($fragment, callable $create)
    {
        return new Fragment(
            array_map($create, $fragment['data']),
            $fragment['currentPage'],
            $fragment['totalCount']
        );
    }

    public function __construct(array $data, int $page, int $total)
    {
        $this->data = $data;
        $this->page = $page;
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
}
