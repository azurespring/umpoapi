<?php

namespace AzureSpring\UmpOpenAPI\Model;

class PeriodicCount
{
    private $moment;

    private $count;

    private $device;

    public static function create($data)
    {
        return new PeriodicCount(new \DateTimeImmutable($data['dateTime']), $data['count'], $data['device']);
    }

    public function __construct(\DateTimeImmutable $moment, int $count, int $device)
    {
        $this->moment = $moment;
        $this->count = $count;
        $this->device = $device;
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getMoment(): \DateTimeImmutable
    {
        return $this->moment;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getDevice(): int
    {
        return $this->device;
    }
}
