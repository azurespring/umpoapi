<?php

namespace AzureSpring\UmpOpenAPI\Model;

class Page
{
    private $index;

    private $size;

    public function __construct(int $index = 1, int $size = 30)
    {
        $this->index = $index;
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    public function toParams()
    {
        return [
            'pageIndex' => $this->index,
            'pageSize' => $this->size,
        ];
    }
}
